
# Importazione configurazione octoprint
Copiare il contenuto del file `config.yaml` nel file `/home/pi/.octoprint/config.yaml`

La configurazione di octoprint viene presa quando si riavvia octoprint.

# Preparare gli script
Caricare tutti i file `OctoPrintAutomation*` nella cartella `/home/pi`

copiare tutti i file `OctoPrintAutomation*` nella cartella `/usr/local/bin` con il comando `sudo find . -name 'OctoPrintAutomation*' -exec mv {} /usr/local/bin/ \;`

eseguire il comando `cd /usr/local/bin` per entrare nella cartella in cui sono contenuti i file

eseguire il comando `sudo find . -name 'OctoPrintAutomation*' -exec chmod a+rx {} \;` per rendere eseguibili i file

sul pin 14 va messo il relè della luce

sul pin 15 va messo il relè per l'alimentazione della stampante

sul pin 18 va messo il relè per l'usb tra raspberry e stampante